## Guia y comnados  Laboratirio Sistema de comunicacion de datos

### Protecciones de las shells

Proteger EXEC con privilegios
```ruby
Sw-Floor-1(config)# enable secret class
Sw-Floor-1(config)# exit
```

Proteger EXEC del usuario(shell user)
```ruby
Sw-Floor-1(config)# line console 0
Sw-Floor-1(config-line)# password cisco
Sw-Floor-1(config-line)# login
```

Cómo proteger el acceso remoto 
```ruby
Sw-Floor-1(config)# line vty 0 15
Sw-Floor-1(config-line)# password cisco
Sw-Floor-1(config-line)# login
```

### Configuracion de un router
```ruby
fullcholas>enable
fullcholas#configure terminal
fullcholas(config)# interface <interface> 
fullcholas(config)# ip address 192.168.1.1 255.255.255.0
fullcholas(config-if) # no shutdown
```

### Enrutamiento estatico

Para el enrutamiento estatico las dos redes tienen que conocerse no basta con que solo un router consoca al otro router

ROUTER 1
configuramos la interfaz de red para poder comunicar los dos routers

```ruby
Router(config)#interface serial 0/0/0
Router(config-if)#ip address 200.100.2.253 255.255.255.252
Router(config-if)#clock rate 64000
Router(config-if)#bandwidth 64
Router(config-if)#no shutdown
Router(config-if)#exit
```

ROUTER 2
```ruby
Router(config)#interface serial 0/0/0
Router(config-if)#ip address 200.100.2.254 255.255.255.252
Router(config-if)#no shutdown
Router(config-if)#exit
```
Agregamos la ruta estatica para que los terminales se puedan comunicar

ROUTER 1
```ruby
Router(config)#ip route 172.168.1.0 255.255.255.0 200.100.2.254(cable serial)
```
ROUTER 2
```ruby
Router(config)#ip route 192.168.1.0 255.255.255.0 200.100.2.254(cable serial)
```

Por ultimo agregamos configuramos el gateway en los terminales para que se puedan comunicar

![alt text](img/conf-gateway.PNG "Gateway default")

Router,Guardado y configuracion
```ruby
copy running-config startup-config 
```
### Enrutamiento dinamico

![alt text](img/redEjemplo_ruteo.png "Gateway default")

La configuración de RIP en R2 será:
```ruby
R2>enable
R2#configure terminal
R2(config)#router rip
R2(config-router)#version 2
R2(config-router)#network 200.1.1.0
R2(config-router)#network 200.2.2.0
R2(config-router)#network 192.168.20.0
```

Referencia:
[CNNA_Desde_Cero](https://www.computernetworkingnotes.com/ccna-study-guide/ip-route-command-explained-with-examples.html)
[CCM-Net](https://es.ccm.net/faq/2759-configuracion-basica-de-un-router-cisco)

[CiscoRip](https://aplicacionesysistemas.com/rip-cisco-version2-de-manera-facil-y-sencilla/)